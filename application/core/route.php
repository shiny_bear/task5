<?php

class Route
{
    static function start()
    {
        //контроллер и действие по умолчанию
        $controller_name = 'Main';
        $action_name = 'index';
        
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        
        //получаем имя контролера
        if ( !empty($routes[2]))
        {
            $controller_name = $routes[2];
        }
        //var_dump($controller_name);
        //получаем имя экшна
        if ( !empty($routes[3]))
        {
            $action_name = $routes[3];
        }   
        //добавляем префиксы
        $model_name = 'Model_'.$controller_name;
        //var_dump($model_name);
        $controller_name = strtolower('Controller_'.$controller_name);
        $action_name = 'action_'.$action_name;
        
        //подцепляем файл с классом модели(файла модели может и не быть)
        
        $model_file = strtolower($model_name).'.php';
        $model_path = "application/models/".$model_file;
        //var_dump($model_path);
        if(file_exists($model_path))
        {
            include "application/models/".$model_file;
            //Почему не модел_пас?
        }
        
        //подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name).'.php';
        $controller_path = 'C:\wamp\www\task5\application\controllers' . '\\' . $controller_file;
        //var_dump($controller_path);
        if(file_exists($controller_path))
        {
            include $controller_path;
        }
        else 
        {
            Route::ErrorPage404();
        }
        
        //создаем контроллер
        //var_dump($controller_name);
        $controller = new $controller_name;
        $action = $action_name;
        session_start();
        
        if(method_exists($controller, $action))
        {
            //вызываем действие контроллера
            $controller->$action();
            //echo('test');
        }
        else
        {
            echo('error is here');
            //var_dump($action);
            Route::ErrorPage404();
        }
    }
        
            
    static public function ErrorPage404()
    {

        //$host = 'http://'.$_SERVER['HTTP_HOST'].'/'.'task5'.'/';
        //header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        //header('Location:'.$host.'404');
        //var_dump($controller_name);
        echo('Error 404 ');
    }    
}


