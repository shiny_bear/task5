<?php

class controller_login extends Controller 
{    
    function __construct() {
        $this->model = new model_login();
        $this->view = new View();
    }
    
    function action_index() 
    {
        $this->model->login();
        $this->view->generate('login_view.php', 'template_view.php');
        header('Location:' . $_POST["url"]);
    }
}