<?php

class Controller_GuestBook extends Controller 
{
    
    function __construct() {
        $this->model = new Model_GuestBook();
        $this->view = new View();
    }
    
    function action_index() 
    {
        $data = $this->model->get_data();
        $this->view->generate('guestbook_view.php', 'template_view.php', $data);   
    }
}