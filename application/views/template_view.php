<!DOCTYPE html>
<html lang='ru'>
    <head> 
        <meta charset="utf-8">
        <title>Main</title>
<!--        <link rel="stylesheet" type="text/css" href="localhost/task5/application/views/style.css">-->
        <style>
            #email {
                margin:5px;
            }
            table {
                width:50%;
                margin-left:25%;
            }
            body {
                background-color: #F5F5DC;
                text-align: center;
            }
            h1{
                text-align: center;
                margin:auto;
            }
            p{
                text-align: center;
                margin:1;
            }
            #auth{
                text-align: center;
                display: block;
                margin-bottom: 0px;
                background-color: #DCDCDC;
                width:20%;
                margin-left:75%;
                position: relative;

            }
        </style>
    </head>
    <body> 
        <?php require_once 'application/views/auth_form.php';
        include 'application/views/'.$content_view; 
        ?>
        <a href='/task5'>Домашняя страница</a>
    </body>
</html> 